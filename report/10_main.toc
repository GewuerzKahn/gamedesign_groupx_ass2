\select@language {english}
\contentsline {chapter}{\numberline {1}Summary}{i}{chapter.1}
\contentsline {chapter}{\numberline {2}Introduction}{1}{chapter.2}
\contentsline {chapter}{\numberline {3}GroupWorkGrades}{2}{chapter.3}
\contentsline {chapter}{\numberline {4}Game rules}{3}{chapter.4}
\contentsline {section}{\numberline {4.1}Setup}{3}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Board setup}{3}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Cards}{4}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Init Game}{5}{subsection.4.1.3}
\contentsline {section}{\numberline {4.2}Turn}{6}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}What the player does each turn}{6}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Additional actions}{6}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Victory condition}{7}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}When the game ends}{7}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}How to win}{7}{subsection.4.3.2}
\contentsline {chapter}{\numberline {5}Discussion}{8}{chapter.5}
\contentsline {section}{\numberline {5.1}Player experiences}{8}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}First play session}{8}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Second play session}{8}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Last play session}{9}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}Review}{10}{section.5.2}
\contentsline {chapter}{\numberline {6}Appendix}{11}{chapter.6}
